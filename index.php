<!DOCTYPE HTML>
<html>

<head>
  <title>My Simple Site</title>
  <meta name="description" content="website description"/>
  <meta name="keywords" content="website keywords, website keywords"/>
  <meta http-equiv="content-type" content="text/html; charset=windows-1252"/>
  <link rel="stylesheet" type="text/css" href="style/style.css" title="style"/>
</head>

<body>
<div id="main">
  <div id="header">
    <div id="logo">
      <div id="logo_text">
        <!-- class="logo_colour", allows you to change the colour of the text -->
        <h1><a href="index.php">MySimple<span
              class="logo_colour">Site</span></a></h1>
      </div>
    </div>
    <div id="menubar">
      <ul id="menu">
        <!-- put class="selected" in the li tag for the selected page - to highlight which page you're on -->
        <li class="selected"><a href="index.php">Home</a></li>
        <li><a href="examples.php">Examples</a></li>
        <li><a href="page.php">A Page</a></li>
        <li><a href="another_page.php">Another Page</a></li>
        <li><a href="contact.php">Contact Us</a></li>
      </ul>
    </div>
  </div>
  <div id="site_content">
    <?php include_once 'sidebar.php';?>
    <div id="content">
      <!-- insert the page content here -->
      <h1>Welcome to My Simple Site</h1>
      <p>Quisque velit nisi, pretium ut lacinia in, elementum id enim. Donec
        sollicitudin molestie malesuada.</p>
      <p>Donec sollicitudin molestie malesuada. Pellentesque in ipsum id orci
        porta dapibus.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum
        dolor sit amet, consectetur adipiscing elit.</p>
      <p>Donec sollicitudin molestie malesuada. Curabitur arcu erat, accumsan id
        imperdiet et, porttitor at sem.</p>
      <p>Curabitur aliquet quam id dui posuere blandit. Cras ultricies ligula
        sed magna dictum porta.</p>
      <h2>Recent Articles</h2>
      <ul>
        <li><a href="#">Article 1</a></li>
        <li><a href="#">Article 2</a></li>
        <li><a href="#">Article 3</a></li>
        <li><a href="#">Article 4</a></li>
        <li><a href="#">Article 5</a></li>
      </ul>
    </div>
  </div>
  <div id="content_footer"></div>
  <div id="footer">
    Copyright &copy; colour_blue | <a
      href="http://validator.w3.org/check?uri=referer">HTML5</a> | <a
      href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a> | <a
      href="http://www.html5webtemplates.co.uk">design from
      HTML5webtemplates.co.uk</a>
  </div>
</div>
</body>
</html>
