<div class="sidebar">
  <!-- insert your sidebar items here -->
  <h3>Latest News</h3>
  <h4>New Website Launched</h4>
  <h5><?php print date('F j, Y'); ?></h5>
  <p><?php print date('Y'); ?> saw the redesign of our website. Take a look
    around and let us know what you think.<br/><a href="#">Read more</a></p>
  <p></p>
  <h3>Useful Links</h3>
  <ul>
    <li><a href="#">link 1</a></li>
    <li><a href="#">link 2</a></li>
    <li><a href="#">link 3</a></li>
    <li><a href="#">link 4</a></li>
  </ul>
  <h3>Search</h3>
  <form method="post" action="#" id="search_form">
    <p>
      <input class="search" type="text" name="search_field"
             value="Enter keywords....."/>
      <input name="search" type="image"
             style="border: 0; margin: 0 0 -9px 5px;"
             src="style/search.png" alt="Search" title="Search"/>
    </p>
  </form>
</div>